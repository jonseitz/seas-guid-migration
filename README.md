#SEAS GUID Migration

A Drupal 7 module the changes the guid value used by the feed importer on the SEAS website directory between the SEAS username and HUID.

Create a new page at `/admin/config/content/migrateguids` where you can select either HUID or SEAS username. Option for a dry run. Outputs a table of migrated values at the end.
